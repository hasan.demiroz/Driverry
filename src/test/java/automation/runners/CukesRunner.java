package automation.runners;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        glue= "src/test/java/autiomation/step_definitions",
        plugin = {
                "json:target/cucumber.json"
        },
        dryRun = false
)

public class CukesRunner {
}

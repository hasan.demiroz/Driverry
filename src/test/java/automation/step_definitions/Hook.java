package automation.step_definitions;

import automation.utilities.Driver;
import io.cucumber.java.After;
import io.cucumber.java.Before;

import java.util.concurrent.TimeUnit;

public class Hook {

    @Before
    public void setUp(){
        System.out.println("Before hook");
        Driver.get().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        System.out.println("After hook");
    }

    @After
    public void teardown() {
        Driver.closeDriver();
    }

}
